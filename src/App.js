import React from 'react';
import { Container } from 'reactstrap';
import List from './components/list/List';

function App() {
    return (
        <div className="App">
            <Container>
                <List />
            </Container>
        </div>
    );
}

export default App;
