import React from "react";
import { Button, Input, Label, FormGroup } from 'reactstrap';

class Item extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editMode: false,
            newValue: this.props.value.value,
        };
    }

    handleChange(event) {
        this.props.handleCheck(this.props.index, event.target.checked);
    }

    setEditMode() {
        this.setState({ editMode: true });
    }

    changeItemText(event) {
        this.setState({ newValue: event.target.value });
    }

    saveText(event) {
        if (event.code === 'Enter') {
            this.props.handleChangeText(this.props.index, this.state.newValue);
            this.setState({ editMode: false });
            return;
        }
        if (event.code === 'Escape') {
            this.setState({
                editMode: false,
                newValue: this.props.value.value,
            });
            return;
        }
    }

    render() {
        return (
            <li className="list-item">
                <FormGroup check>
                    <Input
                        type="checkbox"
                        name="checkbox"
                        value="value"
                        checked={this.props.value.status === 'completed'}
                        onChange={(e) => {this.handleChange(e);}}
                    />
                    {this.state.editMode &&
                        <Input
                            type="text"
                            className="edit-text"
                            value={this.state.newValue}
                            onChange={(e) => {this.changeItemText(e);}}
                            onKeyUp={(e) => {this.saveText(e);}}
                        />
                    }
                    {!this.state.editMode &&
                        <Label
                            check
                            className={this.props.value.status !== 'active' ? 'striked item' : 'item'}
                            onDoubleClick={() => {this.setEditMode()}}
                        >
                            {this.props.value.value}
                        </Label>
                    }
                </FormGroup>
                <Button
                    close
                    className="close"
                    onClick={() => {this.props.handleClickClose(this.props.index);}}
                />
            </li>
        );
    }
}

export default Item;
