import React from "react";
import { Input } from 'reactstrap';
import uniqid from 'uniqid';
import Item from "../item/Item";

class List extends React.Component {
    constructor(props) {
        super(props);
        const list = localStorage.getItem('todo-list');
        this.state = {
            list: list && this.isJsonString(list) ? JSON.parse(list) : [], // { value: '', status: 'active' (or 'completed'), id: uniqid() }
            newValue: '',
            filters: ['all', 'active', 'completed'],
            filter: 'all',
        };
    }

    isJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

    saveList(list) {
        localStorage.setItem('todo-list', list ? JSON.stringify(list) : []);
    }

    handleChange(event) {
        this.setState({ newValue: event.target.value });
    }

    addNewTask(event) {
        if (event.code === 'Enter') {
            const list = this.state.list.slice();
            const newValue = this.state.newValue;
            const id = uniqid();

            const newList = list.concat({
                value: newValue,
                status: 'active',
                id,
            });
            this.setState({
                list: newList,
                newValue: '',
            });
            this.saveList(newList);
        }
    }

    changeTaskStatus(index, checked) {
        this.setState(state => {
            const list = state.list.map((item, i) => {
                if (index === i) {
                    return { ...item, status: checked ? 'completed' : 'active' };
                } else {
                    return item;
                }
            });

            this.saveList(list);
            return {
                list: list,
            };
        });
    }

    handleClickClose(index) {
        const list = this.state.list.slice();
        list.splice(index, 1);
        this.setState({ list: list });
        this.saveList(list);
    }

    changeFilter(event, filter) {
        if (event.target.checked) {
            this.setState({ filter: filter });
        }
    }

    handleChangeText(index, text) {
        this.setState(state => {
            const list = state.list.map((item, i) => {
                if (index === i) {
                    return { ...item, value: text };
                } else {
                    return item;
                }
            });

            this.saveList(list);
            return {
                list: list,
            };
        });
    }

    render() {
        const list = this.state.list;

        return (
            <div>
                <h1>list</h1>

                <Input
                    type="text"
                    onKeyUp={(e) => {this.addNewTask(e);}}
                    onChange={(e) => {this.handleChange(e);}}
                    value={this.state.newValue}
                />
                {this.state.filters.map((filter, index) =>
                    <label className="filter" key={index}>
                        <input
                            type="checkbox"
                            className="filter-checkbox"
                            name="filter"
                            checked={this.state.filter === filter}
                            onChange={(e) => {this.changeFilter(e, filter);}}
                        />
                        {filter}
                    </label>
                )}
                <ul className="list">
                    {list
                        .filter(item => item.status === this.state.filter || this.state.filter === 'all')
                        .map((item, index) =>
                            <Item
                                value={item}
                                index={index}
                                key={index}
                                handleCheck={(index, checked) => {this.changeTaskStatus(index, checked);}}
                                handleClickClose={(index) => {this.handleClickClose(index);}}
                                handleChangeText={(index, text) => {this.handleChangeText(index, text);}}
                            />
                    )}
                </ul>
            </div>
        );
    }
}

export default List;
